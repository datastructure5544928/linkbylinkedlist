/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package queuebylinkedlist;

/**
 *
 * @author informatics
 */
public class Link {  
    public long dData; // data item
    public Link next; // next link in list
// -------------------------------------------------------------

    public Link(long d) // constructor
    {
        dData = d;
    }
// -------------------------------------------------------------

    public void displayLink() // display this link
    {
        System.out.print(dData +" "); }
}
